'use strict'

###*
 # @ngdoc directive
 # @name punchcardApp.directive:day
 # @description
 # # day
###
angular.module('punchcardApp')
  .directive('day', ->
    template: '<div></div>'
    restrict: 'E'
    link: (scope, element, attrs) ->
      element.text 'this is the day directive'
  )
