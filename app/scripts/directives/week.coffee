'use strict'

###*
 # @ngdoc directive
 # @name punchcardApp.directive:week
 # @description
 # # week
###
angular.module('punchcardApp')
  .directive('week', ->
    template: '<div></div>'
    restrict: 'E'
    link: (scope, element, attrs) ->
      element.text 'this is the week directive'
  )
