'use strict'

###*
 # @ngdoc function
 # @name punchcardApp.controller:AboutCtrl
 # @description
 # # AboutCtrl
 # Controller of the punchcardApp
###
angular.module('punchcardApp')
  .controller 'AboutCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
