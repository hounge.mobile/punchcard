'use strict'

###*
 # @ngdoc function
 # @name punchcardApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the punchcardApp
###
angular.module('punchcardApp')
  .controller 'MainCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
