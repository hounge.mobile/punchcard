'use strict'

###*
 # @ngdoc function
 # @name punchcardApp.controller:ResourceCtrl
 # @description
 # # ResourceCtrl
 # Controller of the punchcardApp
###
angular.module('punchcardApp')
.controller 'ResourceCtrl', ($scope) ->
  $scope.awesomeThings = [
    'HTML5 Boilerplate'
    'AngularJS'
    'Karma'
  ]
