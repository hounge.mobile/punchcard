'use strict'

describe 'Controller: ResourceCtrl', ->

  # load the controller's module
  beforeEach module 'punchcardApp'

  ResourceCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ResourceCtrl = $controller 'ResourceCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
