// Generated by CoffeeScript 1.7.1
(function() {
  'use strict';
  describe('Directive: week', function() {
    var scope;
    beforeEach(module('punchcardApp'));
    scope = {};
    beforeEach(inject(function($controller, $rootScope) {
      return scope = $rootScope.$new();
    }));
    return it('should make hidden element visible', inject(function($compile) {
      var element;
      element = angular.element('<week></week>');
      element = $compile(element)(scope);
      return expect(element.text()).toBe('this is the week directive');
    }));
  });

}).call(this);

//# sourceMappingURL=week.map
