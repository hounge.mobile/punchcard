'use strict'

describe 'Directive: day', ->

  # load the directive's module
  beforeEach module 'punchcardApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<day></day>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the day directive'

  it 'should have a month', inject ($compile) ->
    element = angular.element '<day></day>'


  it 'should have a project attached', inject ($compile) ->
    expect(true).toBe true