'use strict'

describe 'Directive: week', ->

  # load the directive's module
  beforeEach module 'punchcardApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<week></week>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the week directive'
